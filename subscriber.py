import json
import os
import pika
import time


def callback(ch, method, properties, body):
    params = json.loads(body)
    print('[x] Received %r' % params)


# Access the CLODUAMQP_URL environment variable and parse it
url = os.environ.get('jellyfish-01.rmq.cloudamqp.com',
                     'amqp://dfexzxjt:rIgwt9mW6mtYtg2ixaeHWMNvaU7NOjk_@jellyfish.rmq.cloudamqp.com/dfexzxjt')
params = pika.URLParameters(url)
# Connect to CloudAMQP
connection = pika.BlockingConnection(params)
# Start a channel
channel = connection.channel()
# Declare a queue
channel.queue_declare(queue='blockchain')
# Set up subscription on the queue
channel.basic_consume('blockchain',
                      callback,
                      auto_ack=True)
# Start consuming messages
print('[*] Waiting for messages. To exit press CTRL+C')
channel.start_consuming()
