## Python Blockchain Peer Network | Flask REST API | RabbitMQ

Start three (or more) terminals and load each ```node.py``` instance in a Google Chrome tab

```$ python3 node.py``` (Defaults to port 3000)

```$ python3 node.py -p 3001``` Additional nodes can be added ```-p 3002``` etc.

```$ python3 subscriber.py``` Creates a consumer for RabbitMQ published messages

In port:3000 (default) Google Chrome instance:

Click ```Network``` tab and add ```localhost:3001```.\
Click ```Wallet & Node``` tab and click ```Create new Wallet``` tab.\
Click ```Mine Coins``` to start with 10 coins in the kitty.\
Click ```Load Blockchain``` to see the blockchain list of block transactions.

In port:3001 Google Chrome instance:

Click ```Network``` tab and add ```localhost:3000```.\
Click ```Wallet & Node``` tab and click ```Create new Wallet``` tab.\
Highlight and copy the public key.

In port:3000 (default) Google Chrome instance:

Paste the port:3001 public key into the ```Recipient Key``` field.\
Enter amount of coins for the transaction and click ```Send```. Funds available are reduced by the amount of the open transaction.\
Check terminal for RabbitMQ ```Message sent to consumer``` log.\
Check ```subscriber.py``` terminal to view received RabbitMQ message.\
Click on ```Open Transactions``` and ```Load Transactions``` to view all open transactions.\
Click ```Blockchain``` and ```Load Blockchain``` to view the the blockchain list of block transactions.\
Click ```Mine Coins``` to commit all open transactions to the blockchain. Funds are increased with a mining reward of 10 coins.

In port:3001 Google Chrome instance:

Click on ```Load Wallet``` to view available balance.\
Click ```Load Blockchain``` to see the blockchain list of block transactions.\
All peer nodes should be in sync but sometimes network conflicts arise and a warning message will be displayed in the notification window. Click ```Resolve Conflicts``` to reset the blockchain in all peer nodes.

Repeat the steps above to send a blockchain transaction to the port:3000 (default) Google Chrome instance or any other additional peer nodes declared in the ```Network``` tab of each Google Chrome node (port number) instance.

The UI interface is created in Vue.js and Bootstrap with an Axios dependency for API endpoint requests. The back-end API is developed in the Flask framework - soon to be ported to the Django REST Framework for further learning challenges.

Code written by Maximilian Schwarzmüller: https://www.udemy.com/course/learn-python-by-building-a-blockchain-cryptocurrency/
